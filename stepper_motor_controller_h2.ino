#include <AccelStepper.h>
#include <Wire.h>
#include "rotator_pins.h"
#include "rotator_config.h"
#include "rs485.h"
#include "watchdog.h"

/******************************************************************************/
enum _rotator_status {
    idle = 1, moving = 2, pointing = 4, error = 8
};
enum _rotator_error {
    no_error = 1, sensor_error = 2, homing_error = 4, motor_error = 8
};

/******************************************************************************/
void cmd_proc(int32_t &setpoint_az, int32_t &setpoit_el);
int32_t az_deg2step(float deg);
int32_t el_deg2step(float deg);
float az_step2deg(int32_t step);
float el_step2deg(int32_t step);
bool isNumber(char *input);

/******************************************************************************/
AccelStepper stepper_az(1, M1IN1, M1IN2), stepper_el(1, M2IN1, M2IN2);
rs485 rs485(RS485_DIR, RS485_TX_TIME);

volatile enum _rotator_status rotator_status = idle;
volatile enum _rotator_error rotator_error = no_error;
static int32_t step_az = 0, step_el = 0;
/******************************************************************************/

void delete_char(char *str, int i) {
    int len = strlen(str);

    for (; i < len - 1 ; i++)
    {
       str[i] = str[i+1];
    }

    str[i] = '\0';
}


void setup() {
    /* Stepper Motor setup */
    stepper_az.setEnablePin(MOTOR_EN);
    stepper_az.setPinsInverted(false, false, true);
    stepper_az.enableOutputs();
    stepper_az.setMaxSpeed(MAX_SPEED);
    stepper_az.setAcceleration(MAX_ACCELERATION);
    stepper_az.setMinPulseWidth(MIN_PULSE_WIDTH);
    stepper_el.setPinsInverted(false, false, true);
    stepper_el.enableOutputs();
    stepper_el.setMaxSpeed(MAX_SPEED);
    stepper_el.setAcceleration(MAX_ACCELERATION);
    stepper_el.setMinPulseWidth(MIN_PULSE_WIDTH);

    /* Serial Communication */
    rs485.begin(BaudRate);

    //rs485.print("Setup...\n\r");

    /* Initialize WDT */
    watchdog_init(WDT_TIMEOUT);
}

void loop() {
    /* Update WDT */
    watchdog_reset();

    /*Read the steps from serial*/
    //rs485.print("Calling cmd_proc...\n\r");
    cmd_proc(step_az, step_el);

    if (rotator_status != error) {
            /*Move the Azimuth & Elevation Motor*/
            stepper_az.moveTo(step_az);
            stepper_el.moveTo(step_el);
            stepper_az.run();
            stepper_el.run();
            rotator_status = pointing;
            if (stepper_az.distanceToGo() == 0 && stepper_el.distanceToGo() == 0) {
                rotator_status = idle;
            }
    } else {
        /* error handler */
        stepper_az.stop();
        stepper_az.disableOutputs();
        stepper_el.stop();
        stepper_el.disableOutputs();
    }
}


/*EasyComm 2 Protocol & Calculate the steps*/
void cmd_proc(int32_t &setpoint_az, int32_t &setpoint_el) {
    /*Serial*/
    char buffer[BufferSize];
    char incomingByte;
    char *Data = buffer;
    char *rawData;
    static uint16_t BufferCnt = 0;
    char data[100];
    String str1, str2, str3, str4, str5, str6;

    /*Read from serial*/
    while (rs485.available() > 0) {
        incomingByte = rs485.read();
        /*new data*/
        if (incomingByte == '\n') {
            buffer[BufferCnt] = 0;
            if (buffer[0] == '!') delete_char(Data,0);
            //Serial.print(buffer[0]);
            if (buffer[0] == 'A' && buffer[1] == 'Z') {
                if (buffer[2] == ' ' && buffer[3] == 'E' && buffer[4] == 'L') {
                    /*Get position*/
                    str1 = String("AZ");
                    str2 = String(az_step2deg(stepper_az.currentPosition()), 1);
                    str3 = String(" EL");
                    str4 = String(el_step2deg(stepper_el.currentPosition()), 1);
                    str5 = String("\n");
                    rs485.print(str1 + str2 + str3 + str4 + str5);
                } else {
                    /*Get the absolute value of angle*/
                    rawData = strtok_r(Data, " ", &Data);
                    strncpy(data, rawData + 2, 10);
                    if (isNumber(data)) {
                        /*Calculate the steps*/
                        setpoint_az = az_deg2step(atof(data));
                    }
                    /*Get the absolute value of angle*/
                    rawData = strtok_r(Data, " ", &Data);
                    if (rawData[0] == 'E' && rawData[1] == 'L') {
                        strncpy(data, rawData + 2, 10);
                        if (isNumber(data)) {
                            /*Calculate the steps*/
                            setpoint_el = el_deg2step(atof(data));
                        }
                    }
                }
            }
            /*Stop Moving*/
            else if (buffer[0] == 'S' && buffer[1] == 'A' && buffer[2] == ' '
                    && buffer[3] == 'S' && buffer[4] == 'E') {
                /*Get position*/
                str1 = String("AZ");
                str2 = String(az_step2deg(stepper_az.currentPosition()), 1);
                str3 = String(" EL");
                str4 = String(el_step2deg(stepper_el.currentPosition()), 1);
                str5 = String("\n");
                rs485.print(str1 + str2 + str3 + str4 + str5);
                setpoint_az = stepper_az.currentPosition();
                setpoint_el = stepper_el.currentPosition();
            }
            /*Reset the rotator*/
            else if (buffer[0] == 'R' && buffer[1] == 'E' && buffer[2] == 'S'
                    && buffer[3] == 'E' && buffer[4] == 'T') {
                /*Get position*/
                str1 = String("AZ");
                str2 = String(az_step2deg(stepper_az.currentPosition()), 1);
                str3 = String(" EL");
                str4 = String(el_step2deg(stepper_el.currentPosition()), 1);
                str5 = String("\n");
                rs485.print(str1 + str2 + str3 + str4 + str5);
            } else if (buffer[0] == 'V' && buffer[1] == 'E') {
                str1 = String("VE");
                str2 = String("SatNOGS-v2");
                str3 = String("\n");
                rs485.print(str1 + str2 + str3);
            } else if (buffer[0] == 'I' && buffer[1] == 'P' && buffer[2] == '0') {
                str1 = String("IP0,");
                str2 = String("Temp not supported");
                str3 = String("\n");
                rs485.print(str1 + str2 + str3);
            } else if (buffer[0] == 'I' && buffer[1] == 'P' && buffer[2] == '1') {
                str1 = String("IP1,");
                str2 = String("Switches not supported");
                str3 = String("\n");
                rs485.print(str1 + str2 + str3);
            } else if (buffer[0] == 'I' && buffer[1] == 'P' && buffer[2] == '2') {
                str1 = String("IP2,");
                str2 = String("Switches not supported");
                str3 = String("\n");
                rs485.print(str1 + str2 + str3);
            } else if (buffer[0] == 'G' && buffer[1] == 'S') {
                str1 = String("GS");
                str2 = String(rotator_status, DEC);
                str3 = String("\n");
                rs485.print(str1 + str2 + str3);
            } else if (buffer[0] == 'G' && buffer[1] == 'E') {
                str1 = String("GE");
                str2 = String(rotator_error, DEC);
                str3 = String("\n");
                rs485.print(str1 + str2 + str3);
            }
            BufferCnt = 0;
            rs485.flush();
        }
        /*Fill the buffer with incoming data*/
        else {
            buffer[BufferCnt] = incomingByte;
            BufferCnt++;
        }
    }
}

/*Convert degrees to steps*/
int32_t az_deg2step(float deg) {
    return (AZ_RATIO * SPR * deg / 360);
}

int32_t el_deg2step(float deg) {
    return (EL_RATIO * SPR * deg / 360);
}

/*Convert steps to degrees*/
float az_step2deg(int32_t step) {
    return (360.00 * step / (SPR * AZ_RATIO));
}

float el_step2deg(int32_t step) {
    return (360.00 * step / (SPR * EL_RATIO));
}

/*Check if is argument in number*/
bool isNumber(char *input) {
    for (uint16_t i = 0; input[i] != '\0'; i++) {
        if (isalpha(input[i]))
            return false;
    }
    return true;
}
